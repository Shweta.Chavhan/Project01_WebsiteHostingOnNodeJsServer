FROM node:16
EXPOSE 9090
WORKDIR /app

COPY ./css ./css
COPY ./js ./js
COPY ./img ./img
COPY ./video ./video
COPY ./index.html ./index.html
COPY ./package.json ./package.json
COPY ./server.js ./server.js
ENTRYPOINT ["node","server.js"] 
