//mime type  => Multipurpose internet mail extensions
//It is label used to identify type of data
//It is used so that software can know how to handle data
//It servers same purpose on the internet that file extensions does on operating system

const http = require("http");
const url = require("url");
const fs = require("fs");
const path = require("path");

const hostname = "127.0.0.1";
const port = 9090;

//Defining Filetypes supported by the server
const mimeTypes = {
  html: "text/html",
  css: "text/css",
  js: "text/javascript",
  jpeg: "image/jpeg",
  jpg: "image/jpg",
  png: "image/png",
  mp4: "video/mp4"
};

http
  .createServer((req, res) => {
    var filename = url.parse(req.url).pathname;
    var filepath = path.join(process.cwd(), unescape(filename)); //uescape removes %20 from fielname if any Browser encodes original url
    console.log("File you are looking for is : " + filepath);
    var loadFile;
    try {
      loadFile = fs.lstatSync(filepath); //loads file/dir
    } catch (error) {
      res.writeHead(404, {
        "Content-Type": "text/plain"
      });
      res.write("404 page not found"); //outputs on browser
      res.end();
      return; //to avoid server crash
    }
    if (loadFile.isFile()) {
      //index.pdf.html  extname gives .pdf.html
      //path.extname(filepath).split(".").reverse()[0]; //gives html
      var mimeType = mimeTypes[path.extname(filepath).split(".").reverse()[0]]; //text/html
      res.writeHead(200, { "Content-Type": mimeType });
      var filestream = fs.createReadStream(filepath);
      filestream.pipe(res);
    } else if (loadFile.isDirectory()) {
      res.writeHead(302, { Location: "index.html" });
      res.end();
    } else {
      res.writeHead(500, { "Content-Type": "text/plain" }); //if neither directory nor file
      res.write("500 Internal server error");
      res.end();
    }
  })
  .listen(port, hostname, () => {
    console.log(`Server is running at ${hostname}:${port}`);
  });
